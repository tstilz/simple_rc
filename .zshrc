
if [ -z "$SSH_TTY" ]; then
    export DBG="/dev/null"
else
    export DBG="/dev/stdout"
fi

echo "loading ZSHRC" > $DBG
echo "Loading $0" > $DBG

### COMMON SETUP ###
SRC_RC=~/b/.myrc
if [ -f $SRC_RC ]; then
    . $SRC_RC > $DBG
fi
### END COMMON SETUP ###

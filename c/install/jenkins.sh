#!/bin/bash

USER=swdev_build
APP=jenkins
CONTAINER=$(sudo /usr/bin/docker ps -aqf "name=$APP")

#docker pull linuxserver/$APP
if [[ -z $CONTAINER ]]; then
	echo "no previous"
else
	echo "Container=$CONTAINER"
	sudo /usr/bin/docker stop $CONTAINER
	sudo /usr/bin/docker rm $CONTAINER
fi

sudo docker create  \
        --name=$APP \
	-v /home/swdev_build/.jenkins:/var/jenkins_home:z \
	-p 49001:8080 \
    jenkins



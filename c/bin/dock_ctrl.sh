#!/bin/bash

CMD=$1
NAME=$2
CONTAINER=$(/usr/bin/docker ps -aqf "name=$NAME")

echo $USER
echo $CONTAINER

if [ -z $CMD ]; then
    CMD="start"
fi

/usr/bin/docker $CMD $CONTAINER
echo $NAME" "$CMD"ed!!"

